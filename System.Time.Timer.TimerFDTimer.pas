unit System.Time.Timer.TimerFDTimer;
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, CTypes;

type
  TFileDescriptor = cint;

  // From linux/time.h.
  TClockID = (
    CLOCK_RealTime = 0,
    CLOCK_Monotonic = 1,
    CLOCK_BootTime = 7,
    CLOCK_RealTime_Alarm = 8,
    CLOCK_BootTime_Alarm = 9
  );

  csize = LongInt;
  cssize = LongWord;
  ctime = LongWord;

  PTimeSpecification = ^TTimeSpecification;
  {$PackRecords C}
  TTimeSpecification = record
    tv_sec: ctime;
    tv_nsec: LongInt;
  end;

  PIntervalTimerSpecification = ^TIntervalTimerSpecification;
  {$PackRecords C}
  TIntervalTimerSpecification = record
    it_interval, it_value: TTimeSpecification;
  end;

  TTimerFDTimer = class
  private
  public
  end;

function timerfd_create(ClockID: TClockID; Flags: cint): TFileDescriptor;
cdecl; external;

function timerfd_settime(
  TimerFD: TFileDescriptor;
  Flags: cint;
  const new_value: PIntervalTimerSpecification;
  old_value: PIntervalTimerSpecification): cint;
cdecl; external;

function timerfd_gettime(
  TimerFD: TFileDescriptor;
  curr_value: PIntervalTimerSpecification): cint;
cdecl; external;

// This is actually just read(2).
function timerfd_read(
  TimerFD: TFileDescriptor;
  Buffer: Pointer;
  Count: csize): cssize;
cdecl; external 'c' name 'read';

function clock_gettime(
  clk_id: TClockID;
  tp: PTimeSpecification): cint;
cdecl; external;

implementation

end.

